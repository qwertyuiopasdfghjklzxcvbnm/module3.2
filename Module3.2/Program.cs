﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Module3_2
{
    static public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("task4");
            Task4 a = new Task4();
            Console.WriteLine("enter a natural number");
            bool task4Bool = a.TryParseNaturalNumber(Console.ReadLine(), out int num4);
            if (task4Bool)
            {
                foreach (int z in a.GetFibonacciSequence(num4))
                    Console.WriteLine(z);
            }



            Console.WriteLine("task5");
            Task5 s = new Task5();
            Console.WriteLine(s.ReverseNumber(-78521));



            Console.WriteLine("task6");
            Task6 d = new Task6();            
            foreach (int z in d.UpdateElementToOppositeSign(new int[] { 1, -2, 3 }))
                Console.WriteLine(z);



            Console.WriteLine("task7");
            Task7 f = new Task7();            
            foreach (int z in f.FindElementGreaterThenPrevious(new int[] { 3, 9, 8, 4, 5, 1 }))
                Console.WriteLine(z);



            Console.WriteLine("task8");
            Task8 g = new Task8();
            int[,] v = g.FillArrayInSpiral(3);
            for (int i = 0; i < v.GetLength(0); i++)
            {
                for (int j = 0; j < v.GetLength(0); j++)
                {
                    Console.Write(v[i, j]+"\t");

                }
               Console.WriteLine();
            }
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result) && result >= 0) return true;
            else return false;
        }

        public int[] GetFibonacciSequence(int n)
        {           
           

            if (n == 0) { return new int[] { }; }
            if (n == 1) { return new int[] { 0 }; }            

            int[] mas = new int[n];
            mas[0] = 0;
            mas[1] = 1;            
            for (int i = 2; n > i; i++)
            {
                mas[i] = mas[i-1] + mas[i -2];               
            }

            return mas;           
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {    
            string a =new string (Math.Abs(sourceNumber).ToString().Reverse().ToArray());
            if(sourceNumber<0) return -int.Parse(a);
            else return int.Parse(a);
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0) return new int[] { };
            else return new int[size];
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {            
            return source.Select(n=>n*-1).ToArray();            
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0) return new int[] { };
            else return new int[size];
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> findElementGreaterThenPrevious = new List<int>();
            for (int i=1;i<source.Length;i++)
            {
                if (source[i] > source[i - 1]) findElementGreaterThenPrevious.Add(source[i]);
            }
            return findElementGreaterThenPrevious;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int num = 1;
            int[,] result = new int[size, size];

            int i = 0, j = 0;          

            while (size != 0)
            {
                int k = 0;
                do 
                { 
                    result[i, j++] = num++; //right

                } while (++k < size - 1);

                for (k = 0; k < size - 1; k++) 
                    result[i++, j] = num++;//down

                for (k = 0; k < size - 1; k++) 
                    result[i, j--] = num++;//left

                for (k = 0; k < size - 1; k++) 
                    result[i--, j] = num++; //up
                //бежим по прямоугольнику 
               
                ++i; ++j;// спускаемся по диагонали и бежим по меньшему прямоугольнику
                size = size < 2 ? 0 : size - 2;
            }
            return result;
        }
    }
}
